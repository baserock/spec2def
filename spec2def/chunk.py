# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import re

from ruamel import yaml

from .spec_utils import *
from .reader import *
from .morphologyDumper import *


class Chunk:
    '''
    Actual chunk morph
    '''

    def __init__(self, spec_file=None, name=None, update_yaml=None,
                 update_rpm=False,update_build=False,macro_expand=True,
                 parse_rules=()):
        '''
        Expects either a spec_file or a spec_reader object
        '''

        self.spec_ref=None
        self.update_yaml=update_yaml
        self.update_rpm=update_rpm
        self.update_build=update_build
        self.name = name

        if spec_file is None:
            raise ValueError("Need to specify a spec file")

        try:
            reader = SpecReader(spec_file, parse_rules=parse_rules)
            self._construct_from_obj(reader, name=name,
                                     macro_expand=macro_expand)
        except IOError:
            logging.error("Spec file not found: %s" % spec_file)
            raise

    def _artifactize_files(self, files, package):
        '''
        Make the files list suitable for pulling all files from an artifact.
        Broadly, this means that files that were in the source tarball won't
        automatically be in the artifact, so we need to make some changes.

        rpm formatting dislikes %doc /foo/bar /baz/quux, so we split all
        %doc and %manifest statements into separate lines.
        '''

        artifactized = []
        for item in files:
            match = re.match(r"\s*%(manifest|doc) ", item)
            if not match:
                artifactized.append(item)
                continue

            words = []
            attr = ""
            for word in item.split()[1:]:
                if word[:6] == "%attr(" and match.group(1) == "doc":
                    attr = "{} ".format(word)
                    continue
                # If it starts with /, it's an absolute path. If it starts
                # with %{foo}/, the macro expands to an absolute path.
                if word.startswith('/') or re.match(r"^%{[^}]+}/", word):
                    if match.group(1) == "doc":
                        artifactized.append("%doc {}{}".format(attr, word))
                    else:
                        raise Exception("Unexpected word '{}' in line '{}' in package '{}'"
                                        .format(word, item, package))
                elif match.group(1) == "manifest":
                    artifactized.append(
                        "%manifest /baserock/RPM/{}".format(word.lstrip('/')))
                elif match.group(1) == "doc":
                    artifactized.append("%doc {}/usr/share/doc/{}/{}".format(
                        attr, package, word))
                else:
                    raise Exception("Unexpected word '{}' in line '{}' in package '{}'"
                                    .format(word, item, package))

        return artifactized

    def _construct_from_obj(self,obj,name=None,macro_expand=True,parse_rules=()):
        '''
        Init class members from SpecReader obj
        '''

        if not isinstance(obj, SpecReader):
            raise ValueError("Error with input arguments spec_file=%s, spec_reader=%s"%(spec_file, spec_reader))

        self.spec_ref=obj

        # If no name was specified, pull one from the parsed spec file
        parse_cache = self.spec_ref.parse(macro_expand=macro_expand)
        if not self.name:
            self.name = parse_cache['name']

        # Prefix affects both build and rpm, and spec2def always does at least
        # one of the two, so do this always.
        self.prefix = obj.getPrefix()

        # Annoying bug with YAML where any additional \n at the end mess up
        # the output
        if self.update_build:
            self.build = list(i for i in obj.getBuildInstructions(join=True)
                              if i and i.strip())
            self.install = list(i for i in obj.getInstallInstructions(join=True)
                                if i and i.strip())
        if self.update_rpm:
            self.rpm_packages = [
                {
                    'name': pkg,
                    'vendor': obj.getPackageHeader(pkg, 'vendor'),
                    'metafile': obj.getMetafile(pkg),
                    'files': self._artifactize_files(obj.getPackageFiles(pkg), pkg),
                    'post': obj.getPackageScript(pkg, 'post'),
                    'postun': obj.getPackageScript(pkg, 'postun'),
                    'pre': obj.getPackageScript(pkg, 'pre'),
                    'preun': obj.getPackageScript(pkg, 'preun'),
                    'version': obj.getPackageHeader(pkg, 'version'),
                    'release': obj.getPackageHeader(pkg, 'release'),
                    'epoch': obj.getPackageHeader(pkg, 'epoch')
                }
                for pkg in obj.getPackageList()
            ]

            # Fields that should be removed if empty/None
            sanitizetypes = [ 'post', 'postun', 'pre', 'preun', 'vendor',
                              'metafile', 'files', 'version', 'release', 'epoch' ]
            for pkg in self.rpm_packages:
                if pkg['name'] == self.name:
                    if not pkg['version']:
                        logging.warn("Package {} has no version!".format(pkg['version']))
                    if not pkg['release']:
                        logging.warn("Package {} has no release!".format(pkg['release']))
                # Insert Provides: information
                provides = []
                for provide in obj.getPackageProvides(pkg):
                    if get_package_by_line(parse_cache, provide.lineno) == pkg['name']:
                        provides.append(provide.value)
                if len(provides) > 0:
                    pkg['provides'] = provides

                # Insert Requires: information
                requires = []
                for require in obj.getRequires():
                    if (get_package_by_line(parse_cache, require.lineno) == pkg['name']
                        and require.rtype == "Requires"):
                        requires.append("{} {}".format(require.name, require.version).strip())
                if len(requires) > 0:
                    pkg['requires'] = requires

                for t in sanitizetypes:
                    if not pkg.get(t):
                        del pkg[t]

            # Remove packages that haven't got either requires or files
            required_fields = set(['files', 'requires', 'version', 'release'])
            self.rpm_packages = [package for package in self.rpm_packages if required_fields.intersection(set(package))]

            self.defines = obj.getDefines()

    def _extract_yaml_blob(self, blob):
        if self.update_yaml:
            # We want to serialize with MorphologyDumper for pretty factor first
            yaml_text = yaml.dump(blob, Dumper=MorphologyDumper, default_flow_style=False, line_break="\n")

            # And reload that blob with the round tripping loader
            blob = yaml.load(yaml_text, yaml.loader.RoundTripLoader)

        return blob

    def yaml(self):
        '''
        Generate YAML/chunk safe dictionary
        '''
        contents = self.update_yaml if self.update_yaml is not None else {}

        if not self.update_yaml:
            contents['name'] = self.name
            contents['kind'] = 'chunk'

        # Prefix affects both build and rpm, and choice is either, or both, so
        # we always need this.
        if self.prefix:
           contents['prefix'] = self.prefix

        if self.update_build:
            section = 'build-commands'
            if any(i.strip() for i in self.install):
                contents['install-commands'] = self._extract_yaml_blob(self.install)
            else:
                # If spec has no install section, move build-commands to install-commands.
                section = 'install-commands'
                del contents['build-commands']

            if any(b.strip() for b in self.build):
                contents[section] = self._extract_yaml_blob(self.build)
            elif contents.get('build-commands'):
                del contents['build-commands']

        if self.update_rpm:
            # Ensure rpm-metadata and add/update the packages section
            meta = contents.get('rpm-metadata', {})

            # In the olden days, rpm-metadata was a list, now it's a dict.
            if not isinstance(meta, dict):
                meta = {}

            meta['packages'] = self._extract_yaml_blob(self.rpm_packages)
            if self.defines:
                meta['defines'] = self.defines

            # Delete deprecated 'version' and 'release' fields
            if 'version' in meta:
                del meta['version']
            if 'release' in meta:
                del meta['release']

            contents['rpm-metadata'] = meta

        return contents

    def dump(self, stream):
        # Use the MorphologyDumper when creating a new
        # file where we want a nice order, if we're updating
        # an existing chunk morph, then use the round trip dumper.
        y = self.yaml()
        if self.update_yaml:
            yaml.round_trip_dump(y, stream)
        else:
            yaml.dump(y, stream, Dumper=MorphologyDumper,
                      default_flow_style=False, line_break="\n")

    def write(self, morph_file):
        '''
        Writes chunk to file. File can be overridden using the morph_file param, but
        is recomended to use the setMorphFile method instead
        '''

        try:
            with open(morph_file, 'w') as f:
                self.dump(f)
        except IOError as io_e:
            raise IOError("Failed to write to morph file", io_e)

        logging.info("Chunk successfully written to {}"
                    .format(os.path.join(morph_file)))
