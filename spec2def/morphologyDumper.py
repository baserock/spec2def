# Copyright (C) 2013-2016  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-2 =*=
#
# Original code can be found at http://git.baserock.org/cgit/baserock/baserock/morph.git/tree/morphlib/morphloader.py
#


from ruamel import yaml


class MorphologyDumper(yaml.SafeDumper):
    keyorder = (
        'name',
        'kind',
        'description',
        'arch',
        'strata',
        'configuration-extensions',
        'morph',
        'repo',
        'ref',
        'unpetrify-ref',
        'build-depends',
        'build-mode',
        'artifacts',
        'max-jobs',
        'submodules',
        'products',
        'chunks',
        'build-system',
        'pre-configure-commands',
        'configure-commands',
        'post-configure-commands',
        'pre-build-commands',
        'build-commands',
        'pre-test-commands',
        'test-commands',
        'post-test-commands',
        'post-build-commands',
        'pre-install-commands',
        'install-commands',
        'post-install-commands',
        'rpm-metadata',
        'artifact',
        'include',
        'systems',
        'deploy-defaults',
        'deploy',
        'type',
        'location',
    )

    @classmethod
    def _iter_in_global_order(cls, mapping):
        for key in cls.keyorder:
            if key in mapping:
                yield key, mapping[key]
        for key in sorted(mapping.iterkeys()):
            if key not in cls.keyorder:
                yield key, mapping[key]

    @classmethod
    def _represent_dict(cls, dumper, mapping):
        return dumper.represent_mapping('tag:yaml.org,2002:map',
                                        cls._iter_in_global_order(mapping))

    @classmethod
    def _represent_str(cls, dumper, orig_data):
        fallback_representer = yaml.representer.SafeRepresenter.represent_str
        try:
            data = unicode(orig_data, 'ascii')
            if data.count('\n') == 0:
                return fallback_representer(dumper, orig_data)
        except UnicodeDecodeError:
            try:
                data = unicode(orig_data, 'utf-8')
                if data.count('\n') == 0:
                    return fallback_representer(dumper, orig_data)
            except UnicodeDecodeError:
                return fallback_representer(dumper, orig_data)
        return dumper.represent_scalar(u'tag:yaml.org,2002:str',
                                       data, style='|')

    @classmethod
    def _represent_unicode(cls, dumper, data):
        if data.count('\n') == 0:
            return yaml.representer.SafeRepresenter.represent_unicode(dumper,
                                                                      data)
        return dumper.represent_scalar(u'tag:yaml.org,2002:str',
                                       data, style='|')

    def __init__(self, *args, **kwargs):
        yaml.SafeDumper.__init__(self, *args, **kwargs)
        self.add_representer(dict, self._represent_dict)
        self.add_representer(str, self._represent_str)
        self.add_representer(unicode, self._represent_unicode)
