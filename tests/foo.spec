Name:		foo
Version:	0.1.0
Release:	1%{?dist}
Summary:        test spec
License:        GPLv2
Vendor:         Foo Tech
Source0:        %{name}-%{version}.tar.bz2
Source1:        foo.conf
Patch0:         some.patch
Patch1:         foo.patch.gz

Buildrequires:  autotools

Requires:       baz
Requires(pre):  foo
Requires(postun): foo
Conflicts:      foobar
Provides:       foo
Obsoletes:      bar

%define major		0
%define somereallylongname foo

%description
foo bar baz
look at me

%package devel
Summary: libs and headers for foo
Epoch: 1
Requires: %{name}

%description devel
this package
contains the foo headers
and library files

%package doc
Summary: docs for foo
BuildArch: noarch
Requires: %{name}

%description doc
Documentation for foo

%prep
%setup -q -n foo
%patch
%patch -P 1 -p1
chmod +x configure

%build
autoreconf -ivf
./configure --prefix=$PREFIX
make

%install
make install DESTDIR=$DESTDIR

%files -f filelist
%manifest foo.manifest
%defattr(-,root,root,-)
/usr/bin/foo
/etc/foo.conf

%files devel
%defattr(-,root,root,-)
/usr/include/*
/usr/lib/*

%files doc
%defattr(-,root,root,-)
/usr/share/doc/*
%doc %attr(0644,root,root) %{_mandir}/man1/*
