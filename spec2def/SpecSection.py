# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import re
import logging


class SpecSection:

    sections=[
        'prep',
        'build',
        'install',
        'check',
        'clean',
        'pre',
        'preun',
        'post',
        'postun',
        'files',
        'changelog',
        'description',
        'package']
    section_type=None
    lineno=None

    def __init__(self, name, line, lineno):
        self.block_buffer=[]

        self.lineno = lineno

        # Parse the section type and optional package from the header line
        parseline = line.lstrip('%')    # Get rid of leading %
        parseline = parseline.rstrip()  # Get rid of trailing whitespace (newline)

        # Split on whitespace, the first word is the type, if there is a second
        # word, it indicates what subpackage this section describes
        linesplit = parseline.split()
        self.section_type = linesplit[0]
        self.interpreter = None
        self.metafile = None
        self.package = None

        # Only type is specified, assume main package
        if len(linesplit) == 1:
            self.package = name

        # Two words specified, assume subpackage
        elif len(linesplit) == 2:
            self.package = name + '-' + linesplit[1]

        # With more than two words, we're down to weird arg parsing now
        elif len(linesplit) > 2:

            linesplit.pop(0)
            while len(linesplit) > 0:

                # A word without an option is a subpackage name
                if linesplit[0][0] != '-':
                    self.package = name + '-' + linesplit[0]
                    linesplit.pop(0)

                # The -n option specifies the package name
                elif linesplit[0] == '-n':
                    self.package = linesplit[1]
                    linesplit.pop(0)
                    linesplit.pop(0)

                # The -p option specifies the interpreter to use
                # for parsing a script body
                #
                elif linesplit[0] == '-p':
                    self.interpreter = linesplit[1]
                    linesplit.pop(0)
                    linesplit.pop(0)

                # The -f option specifies that the specified file should be read
                # from the build directory and it's content appended to the
                # section body. This is used for generating %files lists
                # dynamically.
                elif linesplit[0] == '-f':
                    self.metafile = linesplit[1]
                    linesplit.pop(0)
                    linesplit.pop(0)

                else:
                    logging.warn("Unhandled option '%s' in section header: %s" % (linesplit[0], line.strip()))
                    break

            # Default to main package name if it was not specified in the section header args
            if not self.package:
                self.package = name

        if not self.package:
            raise Exception("Unable to derive package name from section header:\n    %s" % line)

    @staticmethod
    def isType(section, type_str):
        return section != None and section.getType() == type_str

    def getType(self):
        return self.section_type

    def getPackage(self):
        return self.package

    def getInterpreter(self):
        return self.interpreter

    def appendBlock(self,line):
        #TODO any basic formatting here?
        self.block_buffer.append(line)

    def getBuffer(self,stringify=False):
        if not stringify:
            return self.block_buffer
        else:
            return stringify(self.block_buffer)

    @staticmethod
    def match(line):
        #Check all section names

        if line[0] != '%':
            return False

        parseline = line.lstrip('%')    # Get rid of leading %
        parseline = parseline.rstrip()  # Get rid of trailing whitespace (newline)
        linesplit = parseline.split()   # Split on whitespace

        # Check if the first word is one of our section matches
        tochk = linesplit[0]
        if tochk in SpecSection.sections:
            logging.debug("Matched section : %s"%line[1:].strip())
            return True

        #else
        return False

    def __str__(self):
        return unicode(self).encode('utf-8')
    def __unicode__(self):
        return "SpecSection[%s] : %s"%(self.section_type,self.getBuffer(stringify=True))

