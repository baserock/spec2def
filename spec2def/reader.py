# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import os
import re
import io
import shutil
import logging
from subprocess import call, Popen, PIPE

from .SpecHeader import *
from .SpecMacro import *
from .SpecRequires import *
from .SpecSection import *
from .SpecSource import *
from .SpecDefine import *
from . import Patch, PatchManager


class SpecReader:
    '''
    Main class that does all the grunt work of reading+parsing a spec file
    '''

    # Possible args/configs
    ignoreComments = False
    insertAnnotation = True
    skipEmptyNewline = False
    ##

    parse_cache = None
    patch_manager = PatchManager()

    def __init__(self, filename, macro_expand=True, parse_rules=()):
        logging.debug("New SpecReader(%s)" % filename)
        self.filename = filename
        self.parse_cache = None

        self.ignore_lines = set(
            p for rules in parse_rules
            for p in rules.get('ignore-lines', []))
        self.replace_lines = {
            re.compile(k): v for rules in parse_rules
            for k, v in rules.get('replace-lines', {}).items()}
        self.post_replace_lines = {
            re.compile(k): v for rules in parse_rules
            for k, v in rules.get('expand-replace-lines', {}).items()}
        self.code_path = next(
            (rules['code-path'] for rules in reversed(parse_rules)
             if rules.get('code-path') is not None), None)

    def parse(self, macro_expand=True):
        '''
        Parses the spec file, duh. Pass macro_expand to automatically
        expand any of the macro defines (this is actually done by an
        external tool - rpmspec)
        '''

        contents = io.StringIO(
            self._read_spec(self.filename, macro_expand))

        logging.info("Parsing spec file @ %s" % self.filename)

        section_in = None
        sections = []

        shb = SpecHeaderBlock()

        # I had a feeling that parsing line by line was better than doing
        # block parsing of the whole document. Sane or not? you decide
        for lineno, line in enumerate(contents):
            # Strip whitespace before newlines to make yaml.dump happy
            line = re.sub('\s+(?=\n$)', '', line)
            # Remove tabs, yaml forbids them
            line = line.replace('\t', '    ')

            # Deal with empty lines
            if line.strip() == "":
                if section_in and not self.skipEmptyNewline:
                    section_in.appendBlock(line)

            elif (SpecSection.isType(section_in, "description") and
                  not line.startswith('%')):
                section_in.appendBlock(line)

            elif SpecDefine.match(line):
                shb.addHeader(SpecDefine.factory(line))

            elif SpecRequires.match(line):
                shb.addHeader(SpecRequires.factory(line, lineno))

            elif Patch.tag_match(line):
                self.patch_manager.parse_tag(Patch.tag_match(line))

            elif SpecHeader.match(line) and not any(
                    SpecSection.isType(section_in, x)
                    for x in ('prep', 'build', 'install')):
                # TODO sections such as description/package include things
                # that are headers for that subset only. This is currently
                # ignored.
                shb.addHeader(SpecHeader.parse(line, lineno))

            elif SpecSection.match(line):
                # If we were already in a section, push it onto a stack
                if section_in is not None:
                    logging.debug("Section '%s' finished flushing buffer" %
                                  section_in.getType())
                    sections.append(section_in)

                section_in = SpecSection(shb.name, line, lineno)

            elif line.startswith("%patch"):
                patch = self.patch_manager.parse_macro(
                    line, Patch.macro_match(line))
                if section_in.section_type == 'prep':
                    section_in.appendBlock(patch.patch_line())

            elif SpecMacro.match(line) and macro_expand:
                # Hopefully most macros have been delt with if we
                # passed macro_expand to the constructor. Some macros
                # will still exist though, e.g. %setup

                macro = SpecMacro.parse(line.strip(), section_in, shb)

                if macro is not None:
                    logging.debug(macro)

                if isinstance(macro, MacroExpanded):
                    section_in.appendBlock(macro)

            # Else block text while inside a section
            else:
                # Check that we are actually in a section. If not, fail,
                # AFAIK root blocks are only (header) (section)s
                if section_in is None:
                    # Ignore comments (in the spec file, not this one..
                    # that would be just silly)
                    # TODO some comments actually explain context to the
                    #   build instructions is there an inteligent way to
                    #   keep these but ignore, say, commented out code?
                    if line[0:1] == "#":
                        continue

                    # Ignore macros which dont fall into a section when
                    # not expanding macros.
                    if line[0:1] == "%" and not macro_expand:
                        continue

                    err = ("Unmatched block outside of a section : %s" %
                           line.rstrip())
                    logging.warn(err)
                    raise SpecFormatError(err)

                # Add text to section block
                section_in.appendBlock(line)
        # end for each line

        # And don't forget to add the final section we have buffered
        if section_in is not None:
            sections.append(section_in)

        logging.debug("Finshed parsing spec file")

        # Sanity check that we are caching in this obj, not globally/statically
        if self.parse_cache is not None:
            logging.warn("Parse cache already set. "
                         "Either this is an error or you re-run parse()")

        self.parse_cache = {'name': shb.name,
                            'header': shb,
                            'sections': sections}
        return self.parse_cache

    def annotate(self, annotateme, section="all"):
        '''
        Annotates the msg list with comments stating that
        it was automatically generated by this script
        '''

        msglst = list(annotateme)

        if section in ("start", "all"):
            msglst.insert(0, "###\n\n")  # Actually last in block
            msglst.insert(
                0, "# Auto-Generated code block start : spec2def script\n")
            msglst.insert(0, "###\n")    # First in block

        if section in ("end", "all"):
            msglst.append("###\n")
            msglst.append("# End auto-generated code block\n")
            msglst.append("###\n\n")

        return msglst

    def getSection(self, section_name):
        '''
        Shortcut method to grab the block called 'section_name'
        Possible section names in a spec file:
            - Description
            - Build
            - Install
            - Prep
            - ...
        '''

        for sec in self.parse_cache['sections']:
            if sec.getType() == section_name:
                return sec

        raise KeyError("Section not found : %s" % section_name)

    def getName(self):
        '''
        Get the name of the main package, as per what the spec says it is
        '''

        try:
            return self.parse_cache['name']
        except:
            raise Exception("Error: The spec file was missing a name field")

    def getPackageHeader(self, package, header):
        headers = self.parse_cache['header'].getHeaders(header)
        # package may be a macro. Expand %{name}
        package = package.replace(r"%{name}", self.parse_cache['name'])
        for header in headers:
            if get_package_by_line(self.parse_cache, header.lineno) == package:
                return header.value

    def getMetafile(self, package):
        for sec in self.parse_cache['sections']:
            if sec.getPackage() == package and sec.getType() == 'files':
                if sec.metafile:
                    return "/baserock/RPM/" + os.path.basename(sec.metafile)

    def getDefines(self):
        '''
        Get a list of defines as dicts indicating key and value
        '''
        defines = []
        for define in self.parse_cache['header'].defines:
            defines.append({"key": define.key, "value": define.value})
        return defines

    def getPrefix(self):
        '''
        Get the Prefix of the package, as stated in the spec.
        This field is optional, so will return None if it isn't set
        '''
        try:
            return self.parse_cache['header'].getHeader('prefix')
        except KeyError:
            return None

    def getRequires(self):
        '''
        Get the unique set of dependencies. Includes ALL require types
            - BuildRequires
            - Requires
            - Requires(*)
        '''

        return self.parse_cache['header'].getRequires()

    def getBuildPrep(self):
        try:
            prep = list(self.getSection('prep').getBuffer())
            logging.debug("Getting prep section:")
            logging.debug(prep)

        # Assume there was no prep section, so lets just create an empty one
        except:
            prep = []
            logging.debug("No prep section found")

        if not any(p.strip() for p in prep):
            prep = []

        return prep

    def getCodePath(self):
        if self.code_path is not None:
            return self.code_path

        cdpath = self.parse_cache['header'].getCodePath()
        if not cdpath:
            return cdpath

        spec_dir = os.path.dirname(self.filename)
        if os.path.exists(os.path.join(spec_dir, cdpath)):
            if not os.path.isdir(os.path.join(spec_dir, ".git")):
                cdpath = os.path.join(os.path.basename(spec_dir), cdpath)

        return cdpath

    def insertChangePath(self, block):
        # Set up 'cd /path/to' part of the build
        cdpath = self.getCodePath()
        if cdpath:
            block.insert(0, "pushd {}\n".format(cdpath))

    def getMetafilesInstall(self):
        install_lines = set()
        base_path = self.getCodePath() or ""
        for section in self.parse_cache['sections']:
            if section.getType() == 'files' and section.metafile:
                install_lines.add("cp -a {} $DESTDIR/baserock/RPM/\n".format(
                    os.path.join(base_path, section.metafile)))

        if install_lines:
            return ["mkdir -p $DESTDIR/baserock/RPM/\n"] + list(install_lines)
        return install_lines

    def getBuildInstructions(self, annotate=True, raw=False, join=True):
        '''
        Fetches the build instructions, plus optional transforms

        Params
        annotate :  Apply the annotate function (adds in some comments)
        raw :       Do nothing to the internal buffer object
        join :      Internally the buffer is a list of strings. True is
                    suggested if a single combined string is required.
        '''

        prep = self.getBuildPrep()

        try:
            build = self.getSection('build').getBuffer()
            if not any(b.strip() for b in build):
                build = []
        # Assume there was no build section, so lets just create an empty one
        # But this is quite unusual...
        except:
            build = []
            logging.warn("No build section found")

        if prep:
            self.insertChangePath(prep)
            mode = "start" if build else "all"
            prep = self._instructions(prep, annotate, raw, join, mode)
        if build:
            self.insertChangePath(build)
            mode = "end" if prep else "all"
            build = self._instructions(build, annotate, raw, join, mode)

        return prep, build

    def _instructions(self, instruction, annotate=True, raw=False,
                      join=True, annotate_mode="all"):
        # Do no transform at all
        if raw:
            return instruction

        # Add in annotations from this tool
        if annotate and instruction:
            instruction = self.annotate(instruction, annotate_mode)

        # Convert internal data into a single string
        if join:
            instruction = stringify(instruction)

        return instruction

    def insertManifestInstall(self, block):
        # Using a set to prevent duplicate files
        install_dirs = set()
        install_lines = set()
        for package in self.getPackageList():
            for f in self.getPackageFiles(package):
                if not f.strip().startswith("%manifest"):
                    continue
                stripped = f[len("%manifest"):].strip()
                for word in stripped.split():
                    word = word.lstrip('/')
                    install_dirs.add("mkdir -p $DESTDIR/baserock/RPM/{}\n"
                                     .format(os.path.dirname(word)))
                    install_lines.add("cp -a {} $DESTDIR/baserock/RPM/{}\n"
                                      .format(word, os.path.dirname(word)))
        for line in install_lines:
            block.insert(0, line)
        for d in install_dirs:
            block.insert(0, d)

    def insertDocInstall(self, block):
        # Using a set to prevent duplicate lines
        install_dirs = set()
        install_lines = set()
        for package in self.getPackageList():
            for f in self.getPackageFiles(package):
                if not f.strip().startswith("%doc "):
                    continue
                stripped = f[len("%doc "):].strip()
                for word in stripped.split():
                    # Don't need to do anything for files starting with
                    # / or macro expansions.
                    if word[:1] == '/' or re.match("^%(?:{.*}/|attr\()", word):
                        continue
                    dirname = os.path.dirname(word)
                    install_dirs.add("mkdir -p $DESTDIR/usr/share/doc/{}/{}\n"
                                     .format(package, dirname))
                    install_lines.add("cp -a {} $DESTDIR/usr/share/doc/{}/{}\n"
                                      .format(word, package, dirname))
        for line in install_lines:
            block.insert(0, line)
        for d in install_dirs:
            block.insert(0, d)

    def getInstallInstructions(self, annotate=True, raw=False, join=True):
        try:
            install = self.getSection('install').getBuffer()
            if not any(i.strip() for i in install):
                install = []
        except:
            install = []
            logging.debug("No install section found")

        self.insertDocInstall(install)
        self.insertManifestInstall(install)
        filelist = self.getMetafilesInstall()
        if filelist:
            filelist = self._instructions(filelist, annotate, raw, join, "end")
        if install:
            self.insertChangePath(install)
            mode = "start" if filelist else "all"
            install = self._instructions(install, annotate, raw, join, mode)
        return install, filelist

    def getPackageList(self):
        '''
        Fetches the set of packages generated by this specfile
        '''

        # Top-level packages do not necessarily have a %files section,
        # but may still have Requires headers that are of interest.
        # Get the full list of packages by getting the packages of every
        # section, and deduplicating.
        return set([sec.getPackage() for sec in self.parse_cache['sections']])

    def getPackageFiles(self, package_name):
        '''
        Fetches the list of files for a given package name in this spec file
        '''
        for sec in self.parse_cache['sections']:
            if sec.getPackage() == package_name and sec.getType() == 'files':
                return [f.strip() for f in sec.getBuffer() if f.strip()]

        return []

    def getPackageProvides(self, package_name):
        '''
        Fetches the list of all Provides: headers, and returns them as
        dicts (for easy parsing)
        '''
        provides = []
        for header in self.parse_cache['header'].headers:
            if "Provides" in header.key:
                provides.append(header)
        return provides

    def getPackageScript(self, package_name, script_type):
        '''
        Fetches the list of files for a given package name in this spec file
        '''
        script = None
        for sec in self.parse_cache['sections']:
            if sec.getPackage() != package_name:
                continue
            if sec.getType() == script_type:
                # The buffer is an array of lines
                body = sec.getBuffer()
                body = ''.join(body)
                body = body.strip()

                # May specify the script interpreter, with -p in the spec file
                interpreter = sec.getInterpreter()

                script = {}
                if body:
                    script['body'] = body

                if interpreter:
                    script['interpreter'] = interpreter

                break

        return script

    def getSources(self):
        return self.parse_cache['header'].sources

    def _get_basedir(self):
        return os.path.dirname(self.filename)

    def _read_spec(self, specfile, macro_expand=True):
        with io.open(specfile, 'rb') as f:
            contents = f.read().decode("UTF-8")

        processed_lines = io.StringIO()
        for line in contents.splitlines(True):
            newline = re.search("\n$", line)
            if self.ignore_lines:
                # Join ignore patterns into one with |. Ignore patterns must
                # match against the entire line.
                ignore_pattern = re.compile("|".join(
                    i + '$' for i in self.ignore_lines))
                # Remove the line if it matches any ignore_rules.
                if re.match(ignore_pattern, line):
                    continue

            if self.replace_lines:
                for k, v in self.replace_lines.items():
                    if re.search(k, line):
                        line = re.sub(k, v, line)
                        if newline and not line.endswith('\n'):
                            line += '\n'

            processed_lines.write(line)
        contents = processed_lines.getvalue()

        if macro_expand:
            contents = self._expandmacros(contents)
        return contents

    def _expandmacros(self, contents):
        '''
        Expands 'define' macros. We cheat by calling an external tool to
        produce a copy of the spec file with expanded macros
        '''

        logging.debug("Expanding macros using rpmspec")

        try:
            p = Popen(["rpmspec", "-P", "/dev/fd/0"],
                      stderr=PIPE, stdin=PIPE, stdout=PIPE)
            out, err = p.communicate(input=contents.encode('utf-8'))
        except OSError as ose:
            logging.error("Failed to expand macros. "
                          "Do you have the 'rpmspec' binary installed?")
            logging.debug(ose)
            raise

        processed_lines = io.StringIO()
        for line in out.decode("UTF-8").splitlines(True):
            if self.post_replace_lines:
                newline = re.search("\n$", line)
                for k, v in self.post_replace_lines.items():
                    if re.search(k, line):
                        line = re.sub(k, v, line)
                        if newline and not line.endswith('\n'):
                            line += '\n'

            processed_lines.write(line)

        return processed_lines.getvalue()

    def getSpecFile(self):
        return self.filename
