# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import re
import sys
import logging
import argparse

from ruamel import yaml

from spec2def.chunk import Chunk


def _load_yaml(filename):

    try:
        with open(filename) as f:
            # Strip trailing whitespace from lines, so yaml.dump doesn't uglify.
            contents = re.sub(" +(?=\n)", "", f.read(), flags=re.MULTILINE)
            contents = yaml.load(contents, yaml.loader.RoundTripLoader)
    except IOError as e:
        raise Exception("Failed to open file %s: %s" % (filename, str(e)))
    except (yaml.scanner.ScannerError, yaml.composer.ComposerError, yaml.parser.ParserError) as e:
        raise Exception("Malformed YAML:\n\n%s\n\n%s\n" % (e.problem, e.problem_mark))

    if not isinstance(contents, dict):
        raise Exception("YAML file has content of type '%s' instead of expected type 'dict': %s" %
                        (type(contents).__name__, filename))

    return contents


def run():
    parser = argparse.ArgumentParser(description='Best effort attempt at automatically converting a spec file to a morph/definitions file')

    parser.add_argument("-n", "--name",  help='Set name-key for this chunk')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-u", "--update", help='Existing morph file to update')
    group.add_argument("-o", "--output", help='Morph file to create from scratch')

    parser.add_argument("--macro-expand", dest='macro_expand', action='store_true',
                        help='Expand rpm macros, this will use your host installed rpm macros for expansions')

    group = parser.add_mutually_exclusive_group()
    group.add_argument("--rpm", action='store_true',
                       help='Parse and output only rpm metadata from the spec file')
    group.add_argument("--build", action='store_true',
                       help='Parse and output only build/install instructions from the spec file')

    parser.add_argument("-s", "--spec", type=str, help='Path of spec file you wish to convert', required=True)

    parser.add_argument("--parse-rules", action='append',
                        help="yaml file with parse rules")

    args = parser.parse_args()
    logging.debug("args:{}".format(args))

    # if --update-rpm or --update-build were both not specified, it means
    # we want to generate / update both
    #
    if not (args.rpm or args.build):
        args.rpm = True
        args.build = True

    # If we're updating, load the original yaml for the morph we want.
    update_yaml = None
    if args.update:
        try:
            update_yaml = _load_yaml(args.update)
        except Exception as e:
            logging.error("Error loading morph file to update: %s" % str(e))
            sys.exit(1)

    # Resolve the output file to create
    if args.update:
        output_file = args.update
    else:
        output_file = args.output

    # Resolve the spec file to parse
    spec_file = args.spec

    # Read any parse rules
    parse_rules = ()
    def quick_load(filename):
        with open(filename, 'r') as f:
            return yaml.safe_load(f)
    if args.parse_rules:
        parse_rules = tuple(quick_load(rules) for rules in args.parse_rules)
        ignore = next((p['ignore-file'] for p in reversed(parse_rules)
                        if p.get('ignore-file') != None), False)
        if ignore:
            logging.warn("Rules specify chunk should be ignored.  Exiting.")
            sys.exit(0)

    # Parse it
    try:
        mc=Chunk(spec_file=spec_file,
                 name=args.name,
                 update_yaml=update_yaml,
                 update_rpm=args.rpm,
                 update_build=args.build,
                 macro_expand=args.macro_expand,
                 parse_rules=parse_rules)
    except Exception as e:
        logging.error("Error parsing spec file '%s': %s" % (spec_file, str(e)))
        sys.exit(1)

    # Write out the morph file and we're done here
    try:
        mc.write(output_file)
    except Exception as e:
        logging.error("Error writing spec file '%s': %s" % (spec_file, str(e)))
        sys.exit(1)
