# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import re
import logging

from .spec_utils import SpecFormatError


class MacroExpanded:
    original=None
    expanded=None

    def __init__(self,org,ex):
        self.original=org
        self.expanded=ex
    def __str__(self):
        return self.expanded

class SpecMacro:
    regex=r"^%(.+)\s+(.+)?$"
    re_var=r"%{(.+)}"
    re_cmd=r"%(\w*)(\s.*)?$"
    re_fn=r"%(\w*)\((.*)\)"


    name=None
    args=None

    def __init__(self, name, args):
        #No constructor? not used
        self.name=name
        self.args=args

    @staticmethod
    def parse(line, section_ref=None,spec_header=None):
        '''
        Tries to parse a macro and convert it
        '''
        logging.debug("Parsing macro : %s"%line)


        m_var=re.match(SpecMacro.re_var,line)
        m_cmd=re.match(SpecMacro.re_cmd,line)
        m_fn=re.match(SpecMacro.re_fn,line)

        #Find and replace variables
        if m_var:
            #Lookup table?
            logging.debug("VAR %s"%m_var.group(1))

        #Macro commands
        elif m_cmd:
            cmd=m_cmd.group(1)
            #Lookup table?
            logging.debug("CMD %s - %s"%(m_cmd.group(1),m_cmd.group(2)))

            #setup
            if cmd == "setup":
                logging.debug("This is a setup!")

                path_match = None
                if m_cmd.group(2):
                    #If we can find where the codepath is from the setup line
                    # make note of it in the HeaderBlock object
                    re_spec=r"^.*(\-n\s(\S*)).*$"
                    path_match = re.match(re_spec, m_cmd.group(2))

                if path_match:
                    code_path = path_match.group(2)
                else:
                    source = spec_header.getSources()[0].getFile()
                    code_path = re.sub("\.(?:tar\.(?:gz|bz2|xz)|tgz)$", "",
                                       source, flags=re.MULTILINE)
                logging.debug("setup -> cd {}".format(code_path))
                spec_header.setCodePath(code_path)

        #Function commands
        elif m_fn:
            #Lookup table?
            logging.debug("FN %s ( %s )"%(m_fn.group(1),m_fn.group(2)))

        else:
            logging.warn("UNKNOWN macro type %s"%line)

    @staticmethod
    def match(line):
        stripped = line.strip()
        if stripped.startswith("%manifest") or stripped.startswith("%doc"):
            return None
        return re.match(SpecMacro.regex, line)

    @staticmethod
    def factory(line):
        matched=SpecMacro.match(line)
        if matched:
            g=matched.groups()
            return SpecMacro(name=g[0],args=g[1])
        else:
            raise SpecFormatError("String given does not match Macro format : %s"%line)
