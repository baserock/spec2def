# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import os
import sys
import errno
import logging

from ruamel import yaml


class SpecFormatError(Exception):
    pass


def mkdir(dirname):
    try:
        os.mkdir(dirname)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise exc
        pass


def stringify(string, join_char=""):
    return join_char.join(unicode(x) for x in string).strip()


def get_package_by_line(parse_cache, line):
    '''
    Returns the name of the package that a line is in
    '''

    for section in reversed(parse_cache['sections']):
        if (section.section_type == 'package' and section.lineno < line):
            return section.package
    return parse_cache['name']
