# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

"""
test_spec2def
----------------------------------

Tests for `spec2def` module.
"""

import os
from StringIO import StringIO

from spec2def import chunk
from spec2def.main import _load_yaml

test_dir = os.path.dirname(os.path.realpath(__file__))


def _test_file(name):
    return os.path.join(test_dir, name)


def _matches_file(cmp, filepath):
    with open(filepath) as f:
        return f.read() == cmp


def _dump_chunk(chunk):
    stream = None
    try:
        stream = StringIO()
        chunk.dump(stream)
        return stream.getvalue()
    finally:
        if stream:
            stream.close()


def test_build():
    foo = chunk.Chunk(
        name='foo',
        spec_file=_test_file('foo.spec'),
        update_build=True,
        parse_rules=[{'code-path': 'foo'}]
    )
    morph = _dump_chunk(foo)
    expected = _test_file('foo.build.morph')
    assert(_matches_file(morph, expected))


def test_rpm():
    foo = chunk.Chunk(
        name='foo',
        spec_file=_test_file('foo.spec'),
        update_yaml=_load_yaml(_test_file('foo.rpm.morph')),
        update_rpm=True,
        macro_expand=False,
        parse_rules=[{'code-path': 'foo'}]
    )
    morph = foo.yaml()
    expected = _load_yaml(_test_file('foo.rpm.morph'))
    assert(sorted(expected['rpm-metadata']) == sorted(morph['rpm-metadata']))
