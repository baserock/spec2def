# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import re

from .spec_utils import *
from .SpecHeader import *
from .SpecRequires import *
from .SpecSection import *
from .SpecSource import *
from .SpecDefine import *


class SpecHeader:
    key = None
    value = None
    lineno = None

    regex = r"^(\w+)\s*:\s*(.+)$"

    def __init__(self, key=None, value=None, lineno=None):
        self.key = key
        self.value = value
        self.lineno = lineno

    @staticmethod
    def match(line):
        m = re.match(SpecHeader.regex, line)

        if m:
            return m
        else:
            return re.match(SpecRequires.regex, line)

    @staticmethod
    def factory(line, lineno):
        matched = SpecHeader.match(line)
        if matched:
            g = matched.groups()
            return SpecHeader(key=g[0], value=g[1], lineno=lineno)
        else:
            raise SpecFormatError(
                "String given does not match Header format : %s" % line)

    @staticmethod
    def parse(line, lineno):
        if SpecSource.match(line):
            return SpecSource.factory(line)

        # Other, misc header
        if SpecHeader.match(line):
            return SpecHeader.factory(line, lineno)

        # If we are here then we do no have a valid header match
        raise SpecFormatError(
            "String given does not match any known formats : %s" % line)

    def __str__(self):
        return "<%s:%s>" % (self.key, self.value)


class SpecHeaderBlock:
    headers_to_extract = []

    name = None

    def __init__(self):
        self.sources = []
        self.requires = []
        self.headers = []
        self.defines = []

        self.codePath = None

    def addHeader(self, header):
        if isinstance(header, SpecSource):
            self.sources.append(header)
        if isinstance(header, SpecRequires):
            self.requires.append(header)
        if isinstance(header, SpecDefine):
            self.defines.append(header)

        if isinstance(header, SpecHeader):
            # Cache the name when we see it
            if header.key.lower() == "name":
                self.name = header.value
            # TODO other required header fields?

            self.headers.append(header)

    def getRequires(self):
        return self.requires

    def getSources(self):
        return self.sources

    def getName(self):
        return self.name

    def getVendor(self):
        return self.vendor

    def getHeaders(self, search):
        # Get ALL matching headers, because duplicates may exist.
        # Return the objects, because the line number matters.
        headers = []
        for h in self.headers:
            if h.key.lower() == search.lower():
                headers.append(h)
        return headers

    def getHeader(self, search):
        for h in self.headers:
            if h.key.lower() == search.lower():
                return h.value

        raise KeyError("Header '%s' not found" % search)

    def getCodePath(self):
        return self.codePath

    def setCodePath(self, codePath):
        '''
        Sets the path to where code is built from (usually from %prep line)
        '''
        self.codePath = codePath

    @staticmethod
    def _list_str(lst):

        return "[" + (", ".join([str(l) for l in lst])) + "]"

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        strb = u"%s : '%s'\nRequires: %s\nSources: %s\nHeaders: %s " % (
            __name__,
            self.name,
            SpecHeaderBlock._list_str(self.requires),
            SpecHeaderBlock._list_str(self.sources),
            SpecHeaderBlock._list_str(self.headers))

        return strb
