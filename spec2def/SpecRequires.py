# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import re

from .spec_utils import *


class SpecRequires:
    #TODO we dont currently do anything to diferentiate these atm
    BUILD=0
    RUN=1
    POST=2
    PRE=3

    name=None
    version=None
    version_operator=None
    rtype=None
    lineno=None

    regex=r"^((Build)?Requires(\(((post|pre|postun|preun),?)*\))?):\s*([^\s]+)\s*(.*)?$"

    def __init__(self,name=None,version=None, version_operator=None, rtype=None, lineno=None):
        '''
        version_operator: e.g. >= or =
        '''


        self.name=name
        self.version=version
        self.version_operator=version_operator
        self.lineno=lineno

        #Set a default as BUILD
        if rtype is None:
            rtype=self.BUILD

        self.rtype=rtype


    @staticmethod
    def match(requires_str):
        return re.match(SpecRequires.regex, requires_str)

    @staticmethod
    def factory(line, lineno):
        matched=SpecRequires.match(line)
        if matched:
            g=matched.groups()

            return SpecRequires(name=g[5],version=g[6], rtype=g[0], lineno=lineno)
        else:
            raise SpecFormatError("String given does not match Requires format : %s"%line)


    def __str__(self):
        return "Requires %s %s"%(self.name,self.version)
