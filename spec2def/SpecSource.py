# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import re

from .spec_utils import *


class SpecSource:
    '''
    Parses and extracts Source headers from the spec file
    '''

    regex=r"^Source([0-9]*):\s+(.+)\s*$"

    index=None
    source_file=None

    def __init__(self, index, source_file):
        self.index=index
        self.source_file=source_file

    def getFile(self):
        # Source files can be displayed as a url. Whereas RPM only
        # takes the basename as the input filename

        return os.path.basename(self.source_file)

    @staticmethod
    def match(line):
        return re.match(SpecSource.regex, line)

    @staticmethod
    def factory(line):
        matched=SpecSource.match(line)
        if matched:
            g=matched.groups()
            return SpecSource(index=g[0],source_file=g[1])
        else:
            raise SpecFormatError("String given does not match Source format : %s"%line)


    def __str__(self):
        return "[Source] %s @ %s"%(self.index,self.source_file)
