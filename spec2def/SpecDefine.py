# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import re


class SpecDefine:
    key = None
    value = None
    regex = r"^%define\s+(.+)\s+(.+)$"

    def __init__(self, key, value):
        self.key = key
        self.value = value

    @staticmethod
    def match(line):
        return re.match(SpecDefine.regex, line)

    @staticmethod
    def factory(line):
        matched = SpecDefine.match(line)
        if not matched:
            raise SpecFormatError(
                "String given does not match define format: %s" % line)

        return SpecDefine(*matched.group(1, 2))
