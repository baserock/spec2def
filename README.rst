===============================
spec2def
===============================

Tool to convert RPM spec files to Baserock definition files (chunk
morphologies).

Install
-------

Python modules required by this tool:

* ruamel.yaml: On debian like systems, this is the python-ruamel.yaml package.

System tools required:

* rpmspec:  On debian like systems, this is provided by the 'rpm' package.

The exact minimum version required is not known, however it is known to work
with the following:

* 4.11.3    (jessie)
* 4.12.0.12 (sid)

Versions with issues:

* 4.10.0    (wheezy)

Check by running : rpmspec --version


Features
--------

* Automaticlly names chunks based on spec file passed
    * Can manually overide this (-n [NAME])
* Handles %Source inclusion
* Expands RPM macros


Future/wishlist
---------------

* Recurse dependencies
	* Would mean knowing how to find the other repos
* Generate full strata
	* As well as grouped ones using naming convention
* Tests
