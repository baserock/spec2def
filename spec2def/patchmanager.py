# Copyright (C) 2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


import re
import argparse

from .spec_utils import SpecFormatError


_tag_regex = r"^Patch(?P<index>\d*):\s*(?P<filepath>\S+)"
_macro_regex = r"^%patch(?P<suffix>\d*)[^P]*(?:(?<=-)P (?P<param>\d*))?"
_parser = argparse.ArgumentParser(add_help=False)
_parser_options = (
    ('-p', {'dest': 'prefix_strip'}),
    ('-E', {'dest': 'remove_empty', 'action': 'store_true'})
)

for opt in _parser_options:
    _parser.add_argument(opt[0], **opt[1])


class PatchManager:
    def __init__(self):
        self.patches = {}

    def parse_tag(self, tag_match):
        try:
            patch = self.patches.get(
                Patch.index_from_match(tag_match), Patch())
            patch.update_from_tag(tag_match)
            self.patches[patch.index] = patch
            return patch
        except (IndexError, AttributeError):
            raise SpecFormatError("Invalid match object")

    def parse_macro(self, line, macro_match):
        patch = self.patches.get(Patch.index_from_match(macro_match), Patch())
        patch.update_from_macro(line, macro_match)
        self.patches[patch.index] = patch
        return patch


class Patch:
    def __init__(self):
        self.index = None
        self.filepath = None
        self.prefix_strip = 0
        self.remove_empty = False

    @staticmethod
    def tag_match(line):
        return re.match(_tag_regex, line)

    @staticmethod
    def macro_match(line):
        return re.match(_macro_regex, line)

    @staticmethod
    def index_from_match(match):
        try:
            d = match.groupdict()
            if d.get('param') == '':
                raise SpecFormatError("%patch: -P must specify an integer")
            index = d.get('index', d.get('suffix') or d.get('param')) or 0
            return int(index)
        except AttributeError:
            raise SpecFormatError("Invalid match object")

    def update_from_tag(self, tag_match):
        try:
            self.index = Patch.index_from_match(tag_match)
            self.filepath = tag_match.group('filepath')
        except (IndexError, AttributeError):
            raise SpecFormatError("Invalid match object")

    def update_from_macro(self, line, macro_match):
        try:
            if not self.index:
                self.index = Patch.index_from_match(macro_match)
            _parser.parse_known_args(line.split(), namespace=self)
        except (IndexError, AttributeError):
            raise SpecFormatError("Invalid match object")

    def patch_line(self):
        path = '"$(dirs -0)"/{}'.format(self.filepath)
        patch_cmd = "patch -p{}{} -s".format(
            self.prefix_strip, ' -E' if self.remove_empty else '')

        if path.endswith('.gz'):
            cmd = "gzip -dc {} | {}\n".format(path, patch_cmd)
        else:
            cmd = '{} < {}\n'.format(patch_cmd, path)
        return cmd
